from __future__ import with_statement

import logging
import json

from google.appengine.api import files


from google.appengine.ext import webapp, blobstore
#import tempfile
import webapp2
import wsgiref
import generate
from google.appengine.api import memcache
PACKAGE_PREFIX = "com.happyblueduck.lembas  ."

#def zipdir(basedir, archivename):
#    assert os.path.isdir(basedir)
#    with closing(ZipFile(archivename, "w", ZIP_DEFLATED)) as z:
#        for root, dirs, files in os.walk(basedir):
#            #NOTE: ignore empty directories
#            for fn in files:
#                absfn = os.path.join(root, fn)
#                zfn = absfn[len(basedir)+len(os.sep):] #XXX: relative path
#                z.write(absfn, zfn)

class JavaHandler(webapp.RequestHandler):

    def get(self):

        file_name = self.request.get("fileName")
        blob_key = files.blobstore.get_blob_key(file_name)
        reader = blobstore.BlobReader(blob_key)
        t = reader.read()
        self.response.out.write(t)

class MainHandler(webapp.RequestHandler):

    def process(self):
        self.response.headers["Content-Type"] = "application/json"

        generate.HOSTNAME = self.host
        generate.HOSTPORT = str(self.port)

        if self.projectId:
            generate.PROJECTID      = self.projectId

        if self.version:
            generate.VERSION        = self.version

        generate.ENDPOINT       = self.endPoint
        generate.ENDPOINTNAME   = self.project

        if not generate.ENDPOINTNAME:
            generate.ENDPOINTNAME =  self.endPoint.split("/")[-1]

        key = self.endPoint + "/" + self.project + "/" + self.target
        result = memcache.get(key)
        if result is not None:
            self.response.out.write(result)

        elif self.target == "objc" or self.target == 'ios':

            result_json = list()
            interface_source = generate.mainObjC(True)
            result_json.append({"fileName": generate.ENDPOINTNAME+".h", "url": "", "source": interface_source.encode('base64', 'strict')})

            implementation_source = generate.mainObjC(False)
            result_json.append({"fileName": generate.ENDPOINTNAME+".m", "url": "", "source": implementation_source.encode('base64', 'strict')})

            self.response.out.write(json.dumps(result_json))

        elif self.target == "java":

            result = generate.mainJava(self.package)
            self.response.out.write(result)

        elif self.target == "android":

            result = generate.mainAndroid(self.package)
            self.response.out.write(result)

        elif self.target == "dotnet":

            result = generate.mainDotNet(self.endPoint)
            self.response.out.write(result)


        elif self.target == "phonegap":
            result = generate.phoneGap()
            self.response.out.write(result)
        else :
            self.response.out.write("missing target")

        #memcache.add(key=key, value=result, time=120)


        generate.FetchedObject.enums = dict()
        generate.DISCOSERVICEBODY = None

    def post(self):

        data = json.loads(self.request.body)
        generate.DISCOSERVICEBODY = data

        self.host       = data['host']
        self.port       = data['port']
        self.endPoint   = data['endPoint']
        self.target     = data['target']
        self.project    = data['project']
        self.port       = data["port"]
        self.package    = data["package"]
        self.version    = data["version"]
        self.projectId  = data["projectId"]

        self.process()


    def get(self):

        self.parseAttributes()
        self.process()

    def parseAttributes(self):
        self.project     = self.request.get("project")
        self.projectId     = self.request.get("projectId")
        self.projectId     = self.request.get("projectId")

        self.target      = self.request.get("target")
        self.endPoint    = self.request.get("endPoint")
        self.host        = self.request.get("host")
        self.version     = self.request.get("version")

        # optional params
        self.package = PACKAGE_PREFIX+generate.ENDPOINTNAME.lower()
        self.port = "80"
        try:
            self.port = self.request.get("port")
            self.package = self.request.get("package")
        except Exception:
            print ("using default port")



app = webapp2.WSGIApplication([('/java', JavaHandler),('/', MainHandler ) ],
    debug=True)

def main():
    wsgiref.handlers.CGIHandler().run(app)


if __name__ == "__main__":
    main()