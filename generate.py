from StringIO import StringIO
import httplib
import os

import logging
import tempfile
from google.appengine.api import files
from google.appengine.ext import blobstore

__author__ = 'anlcan'

from ConfigParser import ConfigParser

from string import Template
import sys
import json
from ItemTemplates import *
import datetime
import base64

TYPE = "_type"

HOSTNAME = ""
HOSTPORT = ""
ENDPOINT = ""
ENDPOINTNAME = ""  # endpoint is different from ENDPOINT.. ideally ENDPOINT = stuff/prod/v1/ENDPOINTNAME
VERSION = "v1"
PROJECTID = ""

TYPE_REQUEST = "BASE_REQUEST"
TYPE_RESPONSE = "BASE_RESPONSE"
TYPE_OBJECT = "BASE_OBJECT"

DISCOSERVICEBODY = ""
IOS_IGNORE = ['LembasDate', 'LembasFault']


class FetchedObject():
    # CLEAN THE ENUMS AFTER USES
    enums = dict()

    skipObject = ['objectKey', "createDate", 'updateDate', 'status', 'registerId', 'entity']
    skipResponse = ['registerId', 'callbackTag']
    skipRequest = ['endpoint', 'enums', 'session',
                   'verb', 'callTag', 'callbackTag', '_id', 'requestTag', 'deviceId']

    CURRENT_TEMPLATE = None

    INIT = False;
    REQUEST_TEMPLATE = None
    RESPONSE_TEMPLATE = None
    OBJECT_TEMPLATE = None

    def __init__(self, jsonObject):
        """

        """
        # self.type = jsonObject["_name"] # CHANGED from name!!!
        #logging.debug(jsonObject)
        self.type = jsonObject["name"]

        self.rawJson = jsonObject

        self.parseEnums(jsonObject)
        #self.update()

    def parseEnums(self, jsonObject):

        for prop in jsonObject['properties']:
            if prop['propType'] == "MENUM":

                extData = prop['propExtData']
                logging.debug(extData)
                enumName = extData['name']

                if FetchedObject.enums.__contains__(enumName):
                    continue

                enumValues = list()

                for v in extData['values']:
                    enumValues.append(v["name"])

                FetchedObject.enums[enumName] = enumValues


    def update(self):

        jsonContent = getUrl(self.discover + self.type)
        self.rawJson = json.loads(jsonContent)

    def initPhoneGapTemplate(self):
        if FetchedObject.INIT: return;

        FetchedObject.REQUEST_TEMPLATE = readTemplate("PhoneGap/phonegapRequest.template")
        FetchedObject.OBJECT_TEMPLATE = readTemplate("PhoneGap/phonegapModel.template")
        FetchedObject.RESPONSE_TEMPLATE = readTemplate("PhoneGap/phonegapResponse.template")
        FetchedObject.INIT = True

    def getPhoneGap(self):

        obj = self.rawJson
        objMap = {'objectName': obj[TYPE],
                  'projectName': ENDPOINTNAME,
                  'className': self.type,
                  'props': '',
        }

        if self.type.endswith("Response"):
            self.CURRENT_TEMPLATE = self.RESPONSE_TEMPLATE
        elif self.type.endswith("Request"):
            self.CURRENT_TEMPLATE = self.REQUEST_TEMPLATE
        else:
            self.CURRENT_TEMPLATE = self.OBJECT_TEMPLATE

        for propJson in obj['properties']:

            d = dict()
            key = propJson['propName']

            if key == TYPE:
                continue

            if key in FetchedObject.skipRequest:
                continue

            if key in FetchedObject.skipResponse:
                continue

            d['propertyName'] = key
            objMap['props'] += jsPropertyTemplate.substitute(d)

        return self.CURRENT_TEMPLATE.substitute(objMap)


    def getJavaClass(self, addition=None, useGson=False, use_android=False):

        if not addition: addition = dict()
        obj = self.rawJson

        objMap = {'objectName': obj[TYPE],
                  'declarations': '',
                  'inits': '',
                  'accessors': ''}

        objMap.update(addition)
        objMap['className'] = self.type
        ofType = obj["ofType"]

        isRequest = False

        # # caching the template
        if FetchedObject.CURRENT_TEMPLATE is None:
            if use_android:
                template = readTemplate('Java/AndroidClass.template')
            else:
                template = readTemplate('Java/JavaClass.template')

            FetchedObject.CURRENT_TEMPLATE = template

        #if self.type.endswith("Response"):
        if ofType == TYPE_RESPONSE:
            objMap['baseClass'] = 'LembasResponse'

        #elif self.type.endswith("Request"):
        elif ofType == TYPE_REQUEST:
            generic = self.type[:-7] + 'Response'
            if use_android:
                objMap['baseClass'] = 'LembasRequestNG<' + generic + '>'
            else:
                objMap['baseClass'] = 'LembasRequest'

            isRequest = True

        #else:
        else:
            objMap['baseClass'] = 'LembasObject'

        for propJson in obj['properties']:

            key = propJson['propName']
            propType = propJson['propType']

            if key == TYPE:
                logging.warning("\tmissing propType for:" + key)
                continue

            if not propType:
                continue

            if key in FetchedObject.skipObject:
                continue

            # only skip props in request
            if isRequest and key in FetchedObject.skipRequest:
                continue

            d = dict()
            d['propertyName'] = key
            d['propertyNameCapitalized'] = key[0].capitalize() + key[1:]

            if propType.startswith("ARRAYOF_"):
                arrayType = propType.split("_")[1]

                #                if arrayType == "MOBJECT":
                #                    nativeType = propJson['propExtData']
                #                else :
                #                    nativeType = javaTrans[arrayType]

                if javaTrans.has_key(arrayType):
                    nativeType = javaTrans[arrayType]
                else:
                    nativeType = propJson['propExtData']


                # cannot have ArrayList<int> or ArrayList<double>
                if nativeType == "int": nativeType = "Integer"
                if nativeType == "double": nativeType = "Double"

                d['className'] = "ArrayList<%s>" % nativeType
                objMap['declarations'] += javaDeclarationTemplate.substitute(d)
                continue

            elif propType in javaTrans.keys():
                d['className'] = javaTrans[propType]

                objMap['declarations'] += javaDeclarationTemplate.substitute(d)
                objMap['accessors'] += javaSetterTemplate.substitute(d) + javaGetterTemplate.substitute(d)

                continue

            elif propType == "MENUM":
                logging.debug("found enum %s", propJson)
                d['className'] = propJson['propExtData']['name']

                # gson substitute
                if useGson:
                    objMap['declarations'] += javaDeclarationGsonEnumTemplate.substitute(d)
                    objMap['accessors'] += javaSetterGsonTemplate.substitute(d) + javaGetterGsonTemplate.substitute(d)

                else:
                    objMap['declarations'] += javaDeclarationEnumTemplate.substitute(d)

                continue

            else:
                d['className'] = propJson['propExtData']

            objMap['inits'] += javaInitTemplate.substitute(d)
            objMap['declarations'] += javaDeclarationTemplate.substitute(d)
            objMap['accessors'] += javaSetterTemplate.substitute(d) + javaGetterTemplate.substitute(d)

        return FetchedObject.CURRENT_TEMPLATE.substitute(objMap)

    def getObjcClass(self, addition=dict()):

        obj = self.rawJson

        # logging.debugprint ("parsing " , self.type , addition)

        objMap = {'objectName': self.type,
                  'declarations': '',
                  'properties': '',
                  'synthesizes': '',
                  'inits': '',
                  'deallocs': ''}

        objMap.update(addition)

        headerTemp = readTemplate('iOS/Header.template')
        implTemp = None

        parent = self.rawJson['ofType']

        isRequest = False
        isResponse = False

        if parent == 'BASE_RESPONSE':
            objMap['baseClass'] = ENDPOINTNAME + 'Response'
            implTemp = readTemplate('iOS/Response.template')
            isResponse = True

        elif parent == 'BASE_REQUEST':
            objMap['baseClass'] = ENDPOINTNAME + 'Request'
            objMap['response'] = self.type[:-7] + 'Response'

            headerTemp = readTemplate('iOS/RequestHeader.template')
            implTemp = readTemplate('iOS/Request.template')
            isRequest = True
            #else:
        elif parent == 'BASE_OBJECT':
            objMap['baseClass'] = ENDPOINTNAME + 'Object'
            implTemp = readTemplate('iOS/MObject.template')


        #processing properties
        for propJson in obj['properties']:

            key = propJson['propName']
            propType = propJson['propType']

            if key == TYPE:
                continue

            if key in FetchedObject.skipObject:
                continue

            # only skip props in request
            if isRequest and key in FetchedObject.skipRequest:
                continue
            if isResponse and key in FetchedObject.skipResponse:
                continue

            d = dict()

            #print(self.type, key)
            d['propertyName'] = key

            if propType == None:
                raise Exception("none type found: " + str(self.rawJson))

            if propType.startswith("ARRAYOF"):
                d['className'] = "NSMutableArray"

            elif propType in transObject.keys():
                d['className'] = transObject[propType]

            elif propType in transNative.keys():
                d['className'] = transNative[propType]

                objMap['declarations'] += declarationEnumTemplate.substitute(d)
                objMap['properties'] += propertyEnumTemplate.substitute(d)
                objMap['synthesizes'] += synthesizeTemplate.substitute(d)
                continue
            elif propType == "MENUM":
                logging.debug("found enum %s", propJson)
                d['className'] = propJson['propExtData']['name']

                objMap['declarations'] += declarationEnumTemplate.substitute(d)
                objMap['properties'] += propertyEnumTemplate.substitute(d)
                objMap['synthesizes'] += synthesizeTemplate.substitute(d)
                continue

            else:
                d['className'] = propJson['propExtData']

                objMap['inits'] += initTemplate.substitute(d)
                objMap['deallocs'] += deallocTemplate.substitute(d)

            objMap['declarations'] += declarationTemplate.substitute(d)
            objMap['properties'] += propertyTemplate.substitute(d)
            objMap['synthesizes'] += synthesizeTemplate.substitute(d)
            objMap['deallocs'] += deallocTemplate.substitute(d)

        return headerTemp.substitute(objMap), implTemp.substitute(objMap)

    def parseObject(self):
        pass


def findDiscoObjects():
    logging.debug("=== Generating Objects ==")

    resultRaw = None

    if DISCOSERVICEBODY:
        resultRaw = DISCOSERVICEBODY

    else:
        discoKrali = getUrl("/ServiceDisco/")

        logging.debug(discoKrali)
        resultRaw = json.loads(discoKrali.strip())

    objects = list()

    for jsonObject in resultRaw['objects']:

        objectName = jsonObject["name"]
        if objectName in IOS_IGNORE:
            continue

        f = FetchedObject(jsonObject)

        # if f.type.endswith("Carrier"):
        #continue

        objects.append(f)

    return objects


# ######################################################################################################################
########################################################################################################################
#########################################################################################################################

def readTemplate(fileName):
    if not fileName.startswith('templates/'):
        fileName = 'templates/' + fileName

    f = open(fileName, 'r')
    headerTemplate = Template(f.read())
    f.close()
    return headerTemplate


def getServiceHost():
    _hostName = HOSTNAME
    _endPoint = ENDPOINT

    if _hostName.endswith("/"):
        _hostName = _hostName[0:-1]

    protocol = "http"
    port = ":" + HOSTPORT
    if HOSTPORT == "80":
        port = ""

    if _hostName.startswith("http://"):
        _hostName = _hostName[7:]
        protocol = "http"

    if _hostName.startswith("https://"):
        _hostName = _hostName[8:]
        protocol = "https"


    #IGNORING THE PORT ON HOSTNAME
    if _hostName.find(":") > 0:
        _hostName = _hostName.split(":")[0]

    if _endPoint.startswith("/"):
        _endPoint = _endPoint[1:0]

    return '"%s://%s%s/%s/"' % (protocol, _hostName, port, _endPoint)


def phoneGap():
    d = {'file': __file__, 'author': __author__, 'date': datetime.datetime.today(), 'host': getServiceHost()}
    phonegapTemplate = readTemplate("PhoneGap/phonegap.template")

    objects = findDiscoObjects()
    objectClasses = list()
    for fetchedObject in objects:
        fetchedObject.initPhoneGapTemplate()
        objectClasses.append(fetchedObject.getPhoneGap())

    d.update({
        "projectName": ENDPOINTNAME,
        #"enums" : '\n'.join(enums),
        "objects": "\n\n".join(objectClasses)})

    return phonegapTemplate.substitute(d)

def mainAndroid(packageDir):
    """
    TODO WTF!!! REFACTOR THIS WITH JAVA ASAP!
    """

    resultJSON = list()
    objects = findDiscoObjects()

    for enumName, enumValues in FetchedObject.enums.items():
        fileName = enumName + ".java"
        #enumFile = open(fileName, "w")
        package = "package  %s;\n\n" % packageDir
        content = package + javaEnumTemplate.substitute(enumName=enumName, values=",\n".join(enumValues))

        resultJSON.append({"fileName": fileName, "url": fileName, "source": content.encode('base64', 'strict')})

    d = {'file': __file__, 'author': __author__, 'date': datetime.datetime.today(), 'host': HOSTNAME}

    addition = dict()
    addition.update(
        {"imports": "import java.util.*;\n"
                    "import com.happyblueduck.lembas.core.*;\n"
                    "import com.happyblueduck.lembas.commons.*;\n"})
    addition['package'] = packageDir
    addition.update(d)

    serviceHost = '"{0}/{1}/"'.format(HOSTNAME, ENDPOINTNAME)
    addition.update({"serviceHost": serviceHost})

    ## LOCAL STATICS
    fileName = "LocalStatics.java"

    template = readTemplate('Java/LocalStatics.template')
    content = template.substitute(addition)

    resultJSON.append({"fileName": fileName, "url": fileName, "source": content.encode('base64', 'strict')})

    for object in objects:
        fileName = object.type + ".java"
        source = object.getJavaClass(addition=addition, useGson=True, use_android=True)

        resultJSON.append({"fileName": fileName, "url": fileName, "source": source.encode('base64', 'strict')})

    logging.info(json.dumps(resultJSON))
    return json.dumps(resultJSON)


def mainObjC(isHeader=True):
    home = StringIO()

    _endPointVarName = ENDPOINTNAME + "EndPoint"

    serviceHost = getServiceHost()
    d = {'file': __file__, 'author': __author__, 'date': datetime.datetime.today(), 'serviceHost': serviceHost,
         'host': HOSTNAME, 'endPoint': ENDPOINTNAME, 'projectId': PROJECTID,
         "endPointHostName": ENDPOINTNAME + "EndPoint"}
    if isHeader:
        home.write('extern NSString * const %s;\n' % _endPointVarName)

        headerTemplate = readTemplate('iOS/ServiceHeader.template')
        home.write(headerTemplate.substitute(d))

        #for header in imports:
        #    home.write('#import "'+header+'.h"\n')
    else:
        home.write('NSString * const %s\t\t= @"%s/%s";\n\n' % (_endPointVarName, HOSTNAME, ENDPOINTNAME))
        impTemplate = readTemplate("iOS/ServiceImp.template")
        home.write(impTemplate.substitute(d))

    objects = findDiscoObjects()

    if isHeader:
        home.write("\n\n#pragma mark forward class declaration\n\n")
        #forward declarations

        for remoteObject in objects:
            if remoteObject.type.find("`") > 0: continue  # weird .Net stuff gets in our way.
            if remoteObject.type.endswith("Carrier"): continue  # how to not miss StarCraft!!!!

            name = remoteObject.type
            home.write(classTemplate.substitute(className=name))

        home.write("\n\n#pragma mark enum declaration\n\n")
        #enums
        for enumName, enumValues in FetchedObject.enums.items():

            values = list()
            i = 0
            for enumValue in enumValues:
                values.append("\t" + enumName + "_" + enumValue + " = " + str(i))
                i += 1

            home.write(enumTemplate.substitute(values=",\n".join(values), enumName=enumName))

            #class dec+imp
            home.write("\n#pragma mark -\n#pragma mark MObjects\n")

    for o in objects:
        #print (o);
        dec, imp = o.getObjcClass(addition={"endPointHostName": ENDPOINTNAME + "EndPoint", "endPointHost": serviceHost})

        if isHeader:
            home.write(dec)
        else:
            home.write(imp)

    sourceCode = home.getvalue()
    home.close()
    return sourceCode


def zipFile(zipStream, file, len, name):
    # write the contents to the zip file
    #file.seek(0)
    f = StringIO(file.read())
    while True:
        buff = f.read(int(len))
        if buff == "": break
        zipStream.writestr(name, file.read())


def mainJava(packageDir):
    """

    """
    resultJSON = list()
    objects = findDiscoObjects()

    for enumName, enumValues in FetchedObject.enums.items():
        fileName = enumName + ".java"
        #enumFile = open(fileName, "w")
        package = "package  %s;\n\n" % packageDir
        content = package + javaEnumTemplate.substitute(enumName=enumName, values=",\n".join(enumValues))

        resultJSON.append({"fileName": fileName, "url": fileName, "source": content.encode('base64', 'strict')})

    d = {'file': __file__, 'author': __author__, 'date': datetime.datetime.today(), 'host': HOSTNAME}

    addition = dict()
    addition.update(
        {"imports": "import java.util.*;\n"
                    "import com.happyblueduck.lembas.core.*;\n"
                    "import com.happyblueduck.lembas.commons.*;\n"})
    addition['package'] = packageDir
    addition.update(d)

    serviceHost = getServiceHost()
    addition.update({"serviceHost": serviceHost})

    ## LOCAL STATICS
    fileName = "LocalStatics.java"

    template = readTemplate('Java/LocalStatics.template')
    content = template.substitute(addition)

    resultJSON.append({"fileName": fileName, "url": fileName, "source": content.encode('base64', 'strict')})

    for object in objects:
        #if object.type.endswith("Carrier") : continue # how to not miss StarCraft!!!!
        fileName = object.type + ".java"
        source = object.getJavaClass(addition=addition, useGson=True, use_android=False)

        resultJSON.append({"fileName": fileName, "url": fileName, "source": source.encode('base64', 'strict')})

    logging.info(json.dumps(resultJSON))
    return json.dumps(resultJSON)




def getUrl(uri):
    logging.info("connecting %s %s %s %s", str(HOSTNAME), str(HOSTPORT), ENDPOINT, uri)

    fetch_headers = {'Cache-Control': 'no-cache,max-age=0', 'Pragma': 'no-cache'}

    conn = httplib.HTTPConnection(HOSTNAME, HOSTPORT)
    if not uri.startswith("/"):
        uri = "/" + uri

    logging.debug("getting %s", "/" + ENDPOINT + uri)
    conn.request("GET", "/" + ENDPOINT + uri, headers=fetch_headers)
    r1 = conn.getresponse()

    if r1.status != 200:
        logging.debug("response failed for %s", uri, r1.status)

    for header in r1.getheaders():
        logging.debug(header)

    data = r1.read()

    return data
