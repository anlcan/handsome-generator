__author__ = 'anlcan'
from string import Template

################################
#.Net Templatesh
################################
dotNetTrans = {"MSTRING": "String", "MINT": "int", "MDOUBLE": "double", "MBOOL": "bool"}

dotNetDeclarationTemplate = Template(
    '[DataMember(Name = "$propertyName")]\n\tpublic $className $propertyNameCapitalized{ get; set;}\n')
dotNetEnumTemplate = Template("public enum $enumName{\n$values\n}\n")
dotNetInitTemplate = Template("\t\t$propertyNameCapitalized = new $className();\n")


################################
#JAVA Templates
################################
javaTrans = {"MSTRING": "String", "MINT": "int", "MDOUBLE": "double", "MBOOL":
    "boolean", "MLONG": "Long", "MDATE": "LembasDate"}

javaDeclarationTemplate = Template("\tpublic $className $propertyName;\n")
javaDeclarationEnumTemplate = Template("\tpublic $className  $propertyName;\n")
javaDeclarationEnumTemplate = Template("\tpublic $className  $propertyName;\n")


### GSON
javaDeclarationGsonEnumTemplate = Template("\tpublic $className $propertyName;\n")
javaGetterGsonTemplate = Template(
    "\n\tpublic $className get$propertyNameCapitalized(){\n\t\treturn this.$propertyName;\n\t}\n")
javaSetterGsonTemplate = Template(
    "\n\tpublic void set$propertyNameCapitalized($className input){\n\t this.$propertyName = input;\n\t}\n")

javaInitTemplate = Template("\t\tthis.$propertyName =  new $className();\n")

javaSetterTemplate = Template(
    "\n\tpublic void set$propertyNameCapitalized ($className  _$propertyName) {\n\t\tthis.$propertyName = _$propertyName;\n\t}\n")
javaGetterTemplate = Template(
    "\n\tpublic $className get$propertyNameCapitalized () {\n\t\treturn  this.$propertyName;\n\t}\n")

javaEnumTemplate = Template("public enum $enumName{\n$values\n}\n")
javaProcess = """
\tpublic HandsomeResponse process() throws RequestProcessException, UtilSerializeException {
\t\treturn null;
\t}"""

##  ANROID
androidProcess = """
     @Override
    public void run() {
        super.HandsomeRun();
    }
"""

androidRequestImplementation = """implements Runnable"""

################################
# OBJECTIVE-C Templates
################################

#  objects become starred -> NSString *
transObject = {"MSTRING": "NSString", "MDATE": "LembasDate"}
transNative = {"MINT": "NSUInteger", "MDOUBLE": "double", "MBOOL": "BOOL", "MLONG": "long"}

classTemplate = Template("@class $className;\n")
enumTemplate = Template("typedef enum{\n$values\n} $enumName;\n\n")

declarationTemplate = Template("\t$className * $propertyName;\n")
declarationEnumTemplate = Template("\t$className  $propertyName;\n")

propertyTemplate = Template("@property(nonatomic, strong) $className * $propertyName;\n")
propertyEnumTemplate = Template("@property(nonatomic) $className  $propertyName;\n")

initTemplate = Template("\t\tself.$propertyName = [[$className alloc] init];\n")
synthesizeTemplate = Template("@synthesize $propertyName;\n")
deallocTemplate = Template("\t$propertyName = nil;\n")

################################
# PHONE-GAP JAVASCRIPT Templates
################################
jsPropertyTemplate = Template("\tthis.$propertyName = undefined;\n")
